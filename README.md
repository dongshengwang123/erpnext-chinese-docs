# ERPNext中文本地化文档

#### 介绍
ERPNext中文本地化推广相关文档及问题解答，
降低中小企业自主实施ERPNext的门槛
助力中小企业公平、低成本享受开源信息化成果
提升中小企业国际竞争力

 **做两件事** 

ERPNext 选型，系统安装，项目实施，业务功能，日常运维等


- 相关文档
- 问题解答

欢迎对发布的文档提出修订意见，
欢迎提交新问题和解答，我会尽我所能进行解答

本人在discuss.erpnext.com及github.com官网帐号是szufisher

 **此代码库只做问题整理，平台不支持打赏功能，如果此问题库内容有帮到你，感动到你，有打赏冲动，请移步到另一个汉化app https://gitee.com/yuzelin/erpnext_chinese，或直接添加本人微信szufisher 直接打赏， 感谢您对纯开源ERPNext的支持.** 
